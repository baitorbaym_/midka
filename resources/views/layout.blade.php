<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
 
    <title>{{ env('APP_NAME') }}|{{ ($pageTitle)??"" }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
 
             
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
 
                    
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', '  ') }}
                    </a>
                </div>
 
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                 
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>
 
                </div>
            </div>
        </nav>
        <div class="container">
        	@yield('content')
        </div>
    </div>
 
   
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
